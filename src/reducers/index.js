import { combineReducers } from 'redux';

import auth from './auth-reducer';
import employeeForm from './employee-form-reducer';
import employees from './employee-reducer';

export default combineReducers({
  auth,
  employeeForm,
  employees,
});
