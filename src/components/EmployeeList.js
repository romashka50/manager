import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux';
import { map } from 'lodash';

import { fetchEmployee } from '../actions';
import ListItem from './ListItem';

class EmployeeList extends Component {
  componentWillMount() {
    this.props.fetchEmployee();
    this.createDataSource(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.createDataSource(newProps);
  }

  createDataSource({ employees }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(employees);
  }

  renderRow(employee) {
    return <ListItem employee={employee} />;
  }

  render() {
    return (
      <ListView
        dataSource={this.dataSource}
        renderRow={this.renderRow}
      />
    );
  }
}

const mapStateToProps = ({ employees }) => ({
  employees: map(employees, (val, uid) => ({ ...val, uid }))
});

export default connect(mapStateToProps, { fetchEmployee })(EmployeeList);
