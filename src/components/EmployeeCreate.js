import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Card, CardSection, Button } from './common';
import EmployeeForm from './EmployeeForm';
import {
  updateEmployee,
  createEmployee,
} from '../actions';

class EmployeeCreate extends Component {
  onCreatePress() {
    const {
      phone,
      name,
      shift,
    } = this.props;

    this.props.createEmployee({ phone, name, shift: shift || 'Monday' });
  }

  render() {
    return (
      <Card >
        <EmployeeForm />
        <CardSection >
          <Button
            onPress={this.onCreatePress.bind(this)}
          >
            Create
          </Button >
        </CardSection >
      </Card >
    );
  }
}

const mapStateToProps = (state) => {
  const { name, phone, shift } = state.employeeForm;

  return {
    name,
    phone,
    shift
  };
};

export default connect(mapStateToProps, { updateEmployee, createEmployee })(EmployeeCreate);
