import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Picker, Text, View } from 'react-native';

import { CardSection, Input } from './common';
import {
  updateEmployee,
} from '../actions';

class EmployeeCreate extends Component {
  render() {
    const {
      name,
      phone,
      shift,
      employee,
    } = this.props;

    console.log(employee);

    return (
      <View >
        <CardSection >
          <Input
            placeholder="Jone"
            label="Name"
            value={name}
            onChangeText={(value) => this.props.updateEmployee({ prop: 'name', value })}
          />
        </CardSection >
        <CardSection >
          <Input
            placeholder="555-555-555"
            label="Phone"
            value={phone}
            onChangeText={(value) => this.props.updateEmployee({ prop: 'phone', value })}
          />
        </CardSection >
        <CardSection style={{ flexDirection: 'column' }} >
          <Text style={styles.shiftTextStyle} >Shift</Text >
          <Picker
            selectedValue={shift}
            onValueChange={(value) => this.props.updateEmployee({ prop: 'shift', value })}
          >
            <Picker.Item label="Monday" value="Monday" />
            <Picker.Item label="Tuesday" value="Tuesday" />
            <Picker.Item label="Wednesday" value="Wednesday" />
            <Picker.Item label="Thursday" value="Thursday" />
            <Picker.Item label="Friday" value="Friday" />
            <Picker.Item label="Saturday" value="Saturday" />
            <Picker.Item label="Sunday" value="Sunday" />
          </Picker >
        </CardSection >
      </View >
    );
  }
}

const styles = {
  shiftTextStyle: {
    fontSize: 18,
    paddingLeft: 20,
  }
};

const mapStateToProps = (state) => {
  const { name, phone, shift } = state.employeeForm;

  return {
    name,
    phone,
    shift
  };
};

export default connect(mapStateToProps, { updateEmployee })(EmployeeCreate);
