import React from 'react';
import { View, Text, Modal } from 'react-native';

import { CardSection, Button } from './index';

const Confirm = (props) => {
  const {
    children,
    visible,
    onAccept,
    onDecline,
  } = props;
  const {
    containerStyle,
    textStyle,
    sectionStyle
  } = styles;

  return (
    <Modal
      visible={visible}
      transparent
      animationType="slide"
      onRequestClose={() => {}}
    >
      <View style={containerStyle}>
        <CardSection style={sectionStyle}>
          <Text style={textStyle}>{children}</Text >
        </CardSection >
        <CardSection >
          <Button onPress={onAccept} >Yes</Button >
          <Button onPress={onDecline} >No</Button >
        </CardSection >
      </View >
    </Modal >
  );
};

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    justifyContent: 'center',
    position: 'relative'
  },
  textStyle: {
    fontSize: 18,
    lineHeight: 40,
    textAlign: 'center',
    flex: 1
  },
  sectionStyle: {
    justifyContent: 'center'
  }
};

export {
  Confirm
};
