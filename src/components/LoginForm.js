import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View } from 'react-native';

import { Card, CardSection, Input, Button, Spinner } from './common';
import {
  emailChanged,
  passwordChanged,
  loginUser,
} from '../actions';

class LoginForm extends Component {
  onEmailChanged(text) {
    this.props.emailChanged(text);
  }

  onPasswordChanged(text) {
    this.props.passwordChanged(text);
  }

  onButtonPress() {
    const { email, password } = this.props;
    this.props.loginUser({ email, password });
  }

  renderError() {
    if (this.props.error) {
      return (
        <View style={{ backgroundColor: 'white' }}>
          <Text style={styles.errorStyle}>
            {this.props.error}
          </Text>
        </View>
      );
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
        onPress={this.onButtonPress.bind(this)}
      >
        Login
      </Button >
    );
  }

  render() {
    return (
      <Card >
        <CardSection >
          <Input
            onChangeText={this.onEmailChanged.bind(this)}
            placeholder="login@email.com"
            label="Email"
            value={this.props.email}
          />
        </CardSection >
        <CardSection >
          <Input
            onChangeText={this.onPasswordChanged.bind(this)}
            secureTextEntry
            placeholder="password"
            label="Password"
            value={this.props.password}
          />
        </CardSection >
        {this.renderError()}
        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card >
    );
  }
}

const styles = {
  errorStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

const mapStateToProps = ({ auth: { email, password, error, loading } }) => ({
  email,
  password,
  error,
  loading,
});

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  loginUser
})(LoginForm);
