import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import Communication from 'react-native-communications';

import { Card, CardSection, Button, Confirm } from './common';
import EmployeeForm from './EmployeeForm';
import {
  updateEmployee,
  editEmployee,
  deleteEmployee,
} from '../actions';

class EmployeeEdit extends Component {
  state = { showModal: false };

  componentWillMount() {
    _.each(this.props.employee, (value, prop) => {
      this.props.updateEmployee({ prop, value });
    });
  }

  onButtonPress() {
    const {
      phone,
      name,
      shift,
    } = this.props;

    this.props.editEmployee({ phone, name, shift, uid: this.props.employee.uid });
  }

  onTextPress() {
    const {
      phone,
      shift,
    } = this.props;

    Communication.text(phone, `You was hired on ${shift}`);
  }

  render() {
    return (
      <Card >
        <EmployeeForm />
        <CardSection >
          <Button
            onPress={this.onButtonPress.bind(this)}
          >
            Update
          </Button >
        </CardSection >
        <CardSection >
          <Button
            onPress={this.onTextPress.bind(this)}
          >
            Text notify
          </Button >
        </CardSection >
        <CardSection >
          <Button
            onPress={() => this.setState(({ showModal }) => ({ showModal: !showModal }))}
          >
            Fire Employee
          </Button >
        </CardSection >

        <Confirm
          visible={this.state.showModal}
          onAccept={() => this.props.deleteEmployee({ uid: this.props.employee.uid })}
          onDecline={() => this.setState({ showModal: false })}
        >
          Are You sure want to delete this?
        </Confirm>
      </Card >
    );
  }
}

const mapStateToProps = (state) => {
  const { name, phone, shift } = state.employeeForm;

  return {
    name,
    phone,
    shift
  };
};

export default connect(mapStateToProps, {
  updateEmployee,
  editEmployee,
  deleteEmployee,
})(EmployeeEdit);
