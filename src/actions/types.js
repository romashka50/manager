export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAILED = 'login_user_failed';
export const LOGIN_USER_STARTED = 'login_user_started';

export const EMPLOYEE_UPDATE = 'employee_update';
export const EMPLOYEE_CREATE = 'employee_create';
export const EMPLOYEE_FETCH = 'employee_fetch';
export const EMPLOYEE_SAVED_SUCCESS = 'employee_saved';
