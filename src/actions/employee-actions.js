import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import {
  EMPLOYEE_UPDATE,
  EMPLOYEE_CREATE,
  EMPLOYEE_FETCH,
  EMPLOYEE_SAVED_SUCCESS,
} from './types';

export const updateEmployee = ({ prop, value }) => ({
  type: EMPLOYEE_UPDATE,
  payload: {
    prop,
    value,
  }
});

export const createEmployee = ({ phone, name, shift }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase
      .database()
      .ref(`/users/${currentUser.uid}/employees/`)
      .push({ phone, name, shift })
      .then(() => {
        dispatch({
          type: EMPLOYEE_CREATE
        });
        Actions.pop();
      });
  };
};

export const deleteEmployee = ({ uid }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase
      .database()
      .ref(`/users/${currentUser.uid}/employees/${uid}`)
      .remove()
      .then(() => {
        dispatch({
          type: EMPLOYEE_SAVED_SUCCESS
        });
        Actions.pop();
      });
  };
};

export const editEmployee = ({ phone, name, shift, uid }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase
      .database()
      .ref(`/users/${currentUser.uid}/employees/${uid}`)
      .set({ phone, name, shift })
      .then(() => {
        dispatch({
          type: EMPLOYEE_SAVED_SUCCESS
        });
        Actions.pop();
      });
  };
};

export const fetchEmployee = () => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase
      .database()
      .ref(`/users/${currentUser.uid}/employees/`)
      .on('value', snapshot => {
        dispatch({
          type: EMPLOYEE_FETCH,
          payload: snapshot.val()
        });
      });
  };
};
