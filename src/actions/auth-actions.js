import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  LOGIN_USER_STARTED
} from './types';

export const emailChanged = (text) => ({
  type: EMAIL_CHANGED,
  payload: text,
});

export const passwordChanged = (text) => ({
  type: PASSWORD_CHANGED,
  payload: text,
});

export const loginUser = ({ email, password }) => (dispatch) => {
  dispatch({
    type: LOGIN_USER_STARTED
  });

  firebase.auth().signInWithEmailAndPassword(email, password)
    .then(user => loginSuccess(dispatch, user))
    .catch((err) => {
      console.log(err);

      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((user) => loginSuccess(dispatch, user))
        .catch(() => loginFailed(dispatch));
    });
};

const loginSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });

  Actions.main();
};

const loginFailed = dispatch => dispatch({
  type: LOGIN_USER_FAILED
});
