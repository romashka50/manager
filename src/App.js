import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';

import reducers from './reducers';
import Router from './Router';

export default class App extends Component {
  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyD8MeBmErIoIbAjhrYnBUpB75BvG98npKg',
      authDomain: 'manager-8d01c.firebaseapp.com',
      databaseURL: 'https://manager-8d01c.firebaseio.com',
      projectId: 'manager-8d01c',
      storageBucket: 'manager-8d01c.appspot.com',
      messagingSenderId: '713560804556'
    });
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}
